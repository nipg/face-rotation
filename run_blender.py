import sys
import os

pitch_list = [105, 120, 135, 75, 60, 45]
yaw_list = [0]

for pitch in pitch_list:
    for yaw in yaw_list:
        os.system(f"blender --background --python rotate_face.py -- {pitch} {yaw}")
        os.system("killall blender")
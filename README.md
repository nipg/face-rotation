# Artificial Facial Rotation - AFR #

### What is this repository for? ###

* Extraction of face meshes with PRNet
* Rotation and rendering of meshes with Blender 

### Extraction of Face Meshes ###

* Open the ipynb in Google Colab
* Download and save the model weights from: https://drive.google.com/file/d/1UoE-XuW1SDLUjZmJPkIZ1MLxvQFgmTFH/view
* Change the weight, input, and output directory variables in  the notebook
* Run the notebook to extract face meshes for all frames in the input folder via PRNet

### Rotate and Render ###

* Download Blender 2.93.4: https://www.blender.org/download/Blender2.93/blender-2.93.4-linux-x64.tar.xz/
* Add blender to PATH: export PATH=~/blender-2.93.4-linux-x64:$PATH
* Change the data folders in lines 26 and 28 of rotate_face.py
* Change the desired pitch, yaw, and roll in run_blender.py
* Keep in mind that for pitch, due to the camera settings, at 90 the face is full frontal and at 0 it's tilted all the way back
* Execute run_blender.p

import bpy
import os
import math
import random
import sys

scene = bpy.data.scenes["Scene"]
view_layer = bpy.context.view_layer

cameraRotationXAxis = 90
cameraRotationYAxis = 4.5
cameraRotationZAxis = 0

cameraOffsetAtXAxis = 0
cameraOffsetAtYAxis = -248.315
cameraOffsetAtZAxis = 1

argv = sys.argv
argv = argv[argv.index("--") + 1:]  # get all args after "--"
# args [pitch, yaw, folder]
print(argv)  # --> ['example', 'args', '123']

pitch = argv[0]
yaw = argv[1]

PRNet_path = "/home/sean/Face_Rotation/PRNet"

PRNet_output_render = f"/home/sean/Face_Rotation/Rendered_Image_PRNet_{pitch}_{yaw}"

os.system(f'mkdir {PRNet_output_render}')

def Delete_Previous_Things():
    # for obj in bpy.data.objects:
    #     obj.select_set(True)
    #     bpy.ops.object.delete()

    for block in bpy.data.meshes:
        if block.users == 0:
            bpy.data.meshes.remove(block)

    for block in bpy.data.materials:
        if block.users == 0:
            bpy.data.materials.remove(block)

    for block in bpy.data.textures:
        if block.users == 0:
            bpy.data.textures.remove(block)

    for block in bpy.data.images:
        if block.users == 0:
            bpy.data.images.remove(block)

    for block in bpy.data.lights:
        if block.users == 0:
            bpy.data.lights.remove(block)


def Set_Render_Settings():
    scene.render.engine = 'CYCLES'

    scene.cycles.samples = 110

    scene.cycles.preview_samples = 32

    scene.cycles.use_denoising = True

    scene.cycles.device = 'GPU'

    scene.render.resolution_x = 1920
    scene.render.resolution_y = 1080

    scene.render.tile_x = 512
    scene.render.tile_y = 512

    scene.render.use_stamp = 1
    scene.render.stamp_background = (0, 0, 0, 1)
    scene.render.image_settings.file_format = "PNG"
    bpy.context.scene.render.image_settings.quality = 50


def Camera_Initialize():
    camera_data = bpy.data.cameras.new(name='Camera')
    camera_object = bpy.data.objects.new('Camera', camera_data)
    bpy.context.collection.objects.link(camera_object)
    bpy.context.scene.camera = camera_object

    camera_object.location.x = cameraOffsetAtXAxis
    camera_object.location.y = cameraOffsetAtYAxis
    camera_object.location.z = cameraOffsetAtZAxis

    camera_object.rotation_euler = (
    math.radians(cameraRotationXAxis), math.radians(cameraRotationYAxis), math.radians(cameraRotationZAxis))
    camera_object.data.clip_end = 9969.5


def Light_Initialize():
    light_data = bpy.data.lights.new(name="Light", type='SUN')

    # create new object with light datablock
    light_object = bpy.data.objects.new(name="Light", object_data=light_data)

    # link light object to collection
    bpy.context.collection.objects.link(light_object)

    light_object.data.energy = 10
    light_object.data.angle = 3.11541
    light_object.data.cycles.max_bounces = 4
    light_object.data.cycles.cast_shadow = False
    light_object.location[2] = 50

    bpy.ops.object.select_all(action='DESELECT')


def Main_Logic_PRNet():
    for block in bpy.data.objects:
        if block.type == 'MESH':
            bpy.data.objects.remove(block)

    for block in bpy.data.meshes:
        if block.users == 0:
            bpy.data.meshes.remove(block)

    # get list of all files in directory
    file_list = sorted(os.listdir(PRNet_path))

    print(file_list)

    try:
        obj_list = [item for item in file_list if item.endswith('.obj')]

    except:
        obj_list = [item for item in file_list if item.endswith('.fbx')]

    print(obj_list)

    for item in obj_list:

        path_to_file = os.path.join(PRNet_path + '/', item)
        print(path_to_file)

        try:
            bpy.ops.import_scene.obj(filepath=path_to_file)
        except:
            bpy.ops.import_scene.fbx(filepath=path_to_file)

        for obj in bpy.data.objects:
            if obj.type == 'MESH':
                print("Object Detected " + obj.name)
                obj.select_set(True)
                view_layer.objects.active = obj
                bpy.ops.object.origin_set(type='GEOMETRY_ORIGIN', center='BOUNDS')
                obj.rotation_euler[2] = math.radians(45)

    Setup_shader(obj)
    scene.render.filepath = PRNet_output_render


def Setup_shader(obj, basename):
    print("Creating new material")

    # Get material
    mat_Cube = bpy.data.materials.get("FaceTexture")

    if mat_Cube is None:
        # create material
        mat_Cube = bpy.data.materials.new(name="FaceTexture")

    # Assign it to object
    if bpy.context.active_object.data.materials:
        # assign to 1st material slot
        bpy.context.active_object.data.materials[0] = mat_Cube
    else:
        # no slots
        obj.data.materials.append(mat_Cube)

    mat_Cube.use_nodes = True
    my_nodes = mat_Cube.node_tree.nodes

    imageTexShader = my_nodes.new(type='ShaderNodeTexImage')
    base_shader_node = my_nodes.new(type='ShaderNodeBsdfDiffuse')

    imageTexShader.image = bpy.data.images.load(PRNet_path + f"/{basename}_texture.png")

    materialOutput = my_nodes.get('Material Output')

    links = mat_Cube.node_tree.links

    print("Connecting nodes links")

    link = links.new(imageTexShader.outputs[0], base_shader_node.inputs[0])

    link = links.new(base_shader_node.outputs[0], materialOutput.inputs[0])


if __name__ == '__main__':

    Delete_Previous_Things()

    Camera_Initialize()

    Light_Initialize()

    Set_Render_Settings()

    # pitch_list = [0]
    # yaw_list = [0, 15, 30, 45, 60, 75, 90]
    #
    # for pitch in pitch_list:
    #     for yaw in yaw_list:

    file_list = os.listdir(PRNet_path)
    try:
        obj_list = [item for item in file_list if item.endswith('.obj')]

    except:
        obj_list = [item for item in file_list if item.endswith('.fbx')]

    print(obj_list)

    for object in obj_list:
        basename = os.path.splitext(object)[0]

        for block in bpy.data.objects:
            if block.type == 'MESH':
                bpy.data.objects.remove(block)

        for block in bpy.data.meshes:
            if block.users == 0:
                bpy.data.meshes.remove(block)

        path_to_file = os.path.join(PRNet_path + '/', object)
        print(path_to_file)

        try:
            bpy.ops.import_scene.obj(filepath=path_to_file)
        except:
            bpy.ops.import_scene.fbx(filepath=path_to_file)

        for obj in bpy.data.objects:
            if obj.type == 'MESH':
                print("Object Detected " + obj.name)
                obj.select_set(True)
                view_layer.objects.active = obj
                bpy.ops.object.origin_set(type='GEOMETRY_ORIGIN', center='BOUNDS')

		# Yaw, LR, pos goes right
                obj.rotation_euler[2] = math.radians(int(yaw))

                # Pitch, UpDown, pos goes up
                # Automatically at 90, full frontal, 0 is all the way back
                obj.rotation_euler[0] = math.radians(int(pitch))

		#Roll, head tilt
                #obj.rotation_euler[1] = math.radians(45)
                

        Setup_shader(obj, basename)
        scene.render.filepath = f"{PRNet_output_render}/{basename}.png"
        bpy.ops.render.render(write_still=True)

